import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */
public class Mainclass {
    
    public static void main(String[] args) throws IOException {
        String line;
        int amount, credit, choice;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Bank b = new Bank();
        while(true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili\n2) Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä");
            System.out.println("5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta");
            System.out.print("Valintasi: ");
            choice = Integer.parseInt(in.readLine());
            switch(choice) {
                case 0:
                    return;
                case 1:
                    b.addNormalAccount();
                    break;
                case 2:
                    b.addCreditAccount();
                    break;
                case 3:
                    b.deposit();
                    break;
                case 4:
                    b.withdraw();
                    break;
                case 5:
                    b.removeAccount();
                    break;
                case 6:
                    b.search();
                    break;
                case 7:
                    b.printAll();
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
        }
    } 
}

