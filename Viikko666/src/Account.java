

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */ 
public class Account {
    String accountNumber;
    String line;
    int amount, choice;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public Account() {      
        System.out.print("Syötä tilinumero: ");
        try {
            accountNumber = in.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print("Syötä rahamäärä: ");
        try {
            amount = Integer.parseInt(in.readLine());
        } catch (IOException ex) {
            Logger.getLogger(Account.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public String getAccountNumber(){
        return accountNumber;
    }
    public int getAmount(){
        return amount;
    }
    public void setAmount(int a){
        amount = a;
    }
    public void subMoney(int money){
        if(amount >= money){
            amount = amount - money;
        }
    }
    public void print(){
        System.out.println("Tilinumero: "+ accountNumber +" Tilillä rahaa: "+ amount);

        
    }
    public void Money(){
        
    }
}
class NormalAccount extends Account{
    
}

class CreditAccount extends Account{
    int credit;
    
    public CreditAccount() throws IOException{
        System.out.print("Syötä luottoraja: ");
        credit = Integer.parseInt(in.readLine());  
    }
    public void subMoney(int money){
        if(amount+credit >= money){
            amount = amount - money;
        }
    }
    public int getCredit(){
        return credit;
    }
    public void print(){
        System.out.println("Tilinumero: "+ accountNumber +" Tilillä rahaa: "+ amount+" Luottoraja: "+credit);
    }
}