import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */
public class Bank {
    //Account na = new Account();
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    //ArrayList list = new ArrayList();
    private final ArrayList<Account> list;
    
    public Bank() {
        list = new ArrayList();
    }
    
    public void addNormalAccount() throws IOException {
        NormalAccount nAccount = new NormalAccount();
        list.add(nAccount);
        System.out.print("Tili luotu.");

    }
    
    public void addCreditAccount() throws IOException {
        CreditAccount cAccount = new CreditAccount();
        list.add(cAccount);
        System.out.print("Tili luotu.");
    }
    
    public void removeAccount() {
        System.out.print("Syötä poistettava tilinumero: ");
        String line;
        try {
            line = in.readLine();
            for (int i = 0; i < list.size();i++){
                Account someAccount = list.get(i);
                if (someAccount.getAccountNumber().equals(line)){
                    list.remove(someAccount);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Bank.class.getName()).log(Level.SEVERE, null, ex);
    	System.out.println("Tili poistettu.");
        }
    }
    public void search() throws IOException {
        System.out.print("Syötä tulostettava tilinumero: ");
        String line = in.readLine();
        for (int i = 0; i < list.size();i++){
            Account someAccount = list.get(i);
            if (someAccount.getAccountNumber().equals(line)){
                someAccount.print();
            }
        }
    }
    
    public void printAll() {
        System.out.println("Kaikki tilit:");
        for(int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
    
    public void deposit() throws IOException {
        System.out.print("Syötä tilinumero: ");
        String line = in.readLine();
        System.out.print("Syötä rahamäärä: ");
        int money = Integer.parseInt(in.readLine());
        for (int i = 0; i < list.size();i++){
            Account someAccount = list.get(i);
            if (someAccount.getAccountNumber().equals(line)){
                int newMoney = someAccount.getAmount() + money;
                someAccount.setAmount(newMoney);
            }
        }
    }
    
    public void withdraw() throws IOException {
        System.out.print("Syötä tilinumero: ");
        String line = in.readLine();
        System.out.print("Syötä rahamäärä: ");
        int money = Integer.parseInt(in.readLine());
        for (int i = 0; i < list.size();i++){
            Account someAccount = list.get(i);
            if (someAccount.getAccountNumber().equals(line)){
                someAccount.subMoney(money);
            }
        }
    }
}

