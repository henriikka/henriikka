/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juoma.automaatti;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

/**
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {
    private BottleDispenser bd;
    @FXML
    private Label label;
    @FXML
    private Button addMoneybutton;
    @FXML
    private Button buyBotton;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Label countLabel;
    @FXML
    private Slider countSlider;
    @FXML
    private Label moneyOutput;
    @FXML
    private Label returnOutput;
    @FXML
    private ComboBox<String> chooseBrand;
    @FXML
    private ComboBox<String> chooseSize;
    @FXML
    private Label infoLabel;
    @FXML
    private Font x1;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bd = BottleDispenser.getInstance();
        ArrayList options = new ArrayList();
        options.add("0.5");
        options.add("1.5");
        chooseSize.setItems(FXCollections.observableList(options));
        ArrayList list = new ArrayList();
        list.add("Pepsi Max");
        list.add("Coca-Cola Zero");
        list.add("Fanta Zero");
        
        chooseBrand.setItems(FXCollections.observableList(list));
    }   

    @FXML
    private void addMoneyButtonAction(ActionEvent event) {
        String test = countLabel.getText();
        bd = BottleDispenser.getInstance();
        bd.addMoney(countSlider.getValue());  
        printMoney();
        countSlider.setValue(0);
        countLabel.setText("  ");
    }
    
    private void printMoney(){
        moneyOutput.setText(String.format("%.1f", bd.getMoney()));
    }
    
    @FXML
    private void buyAction(ActionEvent event) throws IOException {
        Bottle b = new Bottle(chooseBrand.getValue(), Double.parseDouble(chooseSize.getValue()),1.50);
        if (bd.getMoney() < 1.50){
            infoLabel.setText("Syötä rahaa ensin!");
        }
        //else if(checkBottle(b)==false) {
          //  infoLabel.setText("Pullot ovat loppuneet!");
        else { 
            infoLabel.setText(bd.buyBottle(b));
            printMoney();
        }
       
        
    }

    @FXML
    private void returnMoneyAction(ActionEvent event) {
        returnOutput.setText("Palautetaan "+ (String.format("%.1f", bd.getMoney())) + "€");
        bd.returnMoney();
        printMoney();
        
    }

    @FXML
    private void putMoneyAction(MouseEvent event) {
        countLabel.setText(String.format("%.1f", countSlider.getValue()));
    }

    private boolean checkBottle(Bottle b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
