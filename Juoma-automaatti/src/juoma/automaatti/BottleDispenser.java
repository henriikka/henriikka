//import java.util.ArrayList;
package juoma.automaatti;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *Henriikka Harinen 0520995
 * NetBeans IDE 8.1
 * 29.5.2018
 */
public class BottleDispenser implements java.io.Serializable{
    private int bottles;
    private double money;
    private int choise;
    private ArrayList <Bottle> list = new ArrayList();
    static private  BottleDispenser bd = null;
    private BottleDispenser(){
        list = listBottles();
    }
    static BottleDispenser getInstance(){
        if (bd == null)
            bd = new BottleDispenser();
        return bd;
    }
    public ArrayList listBottles() {
        ArrayList<Bottle> list = new ArrayList();
        list.add(new Bottle());
        list.add(new Bottle("Pepsi Max",1.5,1.5));
        list.add(new Bottle("Coca-Cola Zero",0.5,1.5));
        list.add(new Bottle("Coca-Cola Zero",1.5,1.5));
        list.add(new Bottle("Fanta Zero",0.5,1.5));
        list.add(new Bottle("Fanta Zero",0.5,1.5));
        return list;
    }           
    public void printList(){
        for (int i = 0; i < list.size(); i++){
        System.out.println((i+1)+". "+"Nimi: "+list.get(i).getName());
        System.out.println("	Koko: "+list.get(i).getSize()+"	Hinta: "+list.get(i).getPrice());        
        }
    
    }
    
    public String buyBottle(Bottle b) throws IOException {
        String s;
        s = "a";
        for (int i = 0; i < list.size();i++){
            if (list.get(i).getName().equals(b.getName()) && list.get(i).getSize() == b.getSize()){
                list.remove(i);
                money -= 1.50;
                s = ("KACHUNK! " + b.getName() + " tipahti masiinasta!");
                BufferedWriter out;
                out = new BufferedWriter(new FileWriter("kuitit.txt"));
                out.write("kuitti: "+b.getName()+b.getPrice());
                out.close();
                break;
            }
            else{
                s = ("Tuotetta ei saatavilla");
                
            }  
        }
        return s;
        
    }
    
    
    /*public BottleDispenser() {
        bottles = 5;
        money = 0;
        //bottle_array = new Bottle[bottles];
        //for(int i = 0; i<bottles; i++){
          //  bottle_array[i] = new Bottle();
        //}
    }*/
    
    public void addMoney(double d) {
        money += d;
        //System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %4.2f€\n",money);
        money = 0;
       }
    public double getMoney(){
        return money;
    }
}



