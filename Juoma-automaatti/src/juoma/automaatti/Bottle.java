/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juoma.automaatti;
/**
 *Henriikka Harinen 0520995
 * NetBeans IDE 8.1
 * 29.5.2018
 */

public class Bottle {
    private Bottle[] bottles = new Bottle[100];
    private final String manufacturer;
    private final String name;
    private final double total_energy;
    public final double size;
    public final double price;
    
    public Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.80;
    }
    
    public Bottle(String n, double s, double p){
        name = n;
        manufacturer = "pepsi";
        total_energy = 1;
        size = s;
        price = p;
    
    }

    public String getName(){
        return name;
    }

    public double getPrice(){
        return price;
    }    
    public String getManufacturer(){
        return manufacturer;
    }
    
    public double getEnergy(){
        return total_energy;
    }
    public double getSize(){
        return size;
    }    
}   
    
    /*public void addBottle(Bottle b){
        if (bottleCount < 100){    
            bottles[bottleCount++] = b;
        }
    }
    
    public Bottle getBottle(int i) {
        return bottles[i];
    }
}
*/