/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.util.ArrayList;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * HenriikkaHarinen 0520994
 */
public class ShapeHandler implements java.io.Serializable{
    static private ShapeHandler sh = null;
    ArrayList<Point> list = new ArrayList();
    ArrayList<Liney> Llist = new ArrayList();

    private ShapeHandler(){        
    }
    
    static ShapeHandler getInstance(){
        if(sh == null)
            sh = new ShapeHandler();
        return sh;
    }
    
    public void addList(Point p){
        list.add(p);
    }
    public ArrayList getList(){
        return list;        
    }
    public void checkList(AnchorPane base, boolean dunno){
        ArrayList<Point> listy = new ArrayList();
        
        for(int i = 0; i < list.size(); i++){
            if (list.get(i).getMemory() == true) {
                listy.add(list.get(i));
                
            }
        }
        System.out.println(Llist.size());
        if(listy.size() >= 2){
            if (dunno == true) {
                for(int j = 0; j < Llist.size(); j++){
                    base.getChildren().remove(Llist.get(j).getLine());
                }
                Llist.clear();
            }
            for(int i = 0; i < listy.size(); i++){
                listy.get(i).setMemory(false);
                System.out.println(listy.get(i).getCircle().getCenterX() +", "+listy.get(i).getCircle().getCenterY());
            }
            
            Line lineypiney = drawLine(listy.get(0),listy.get(1));
            base.getChildren().add(lineypiney);
            //System.out.println(lineypiney.getStartX() +", "+lineypiney.getStartY());
            //System.out.println(lineypiney.getEndX() +", "+lineypiney.getEndY());
        }         
    }

    public Line drawLine(Point point1,Point point2) {
        Liney ln = new Liney(point1, point2);
        //System.out.println(point1.getCircle().getCenterX() +", "+point1.getCircle().getCenterY());
        //System.out.println(lineypiney.getEndX() +", "+lineypiney.getEndY());
        Llist.add(ln); 
        return ln.getLine();
    }
}
