package viikko11;



import static javafx.scene.paint.Color.BLUE;
import javafx.scene.shape.Line;


/**
 *
 * @author p7973
 */
public class Liney{
    String name;
    final Line l = new Line();


    public Liney(Point point1, Point point2){
        double point1X = point1.getCircle().getCenterX();
        double point1Y = point1.getCircle().getCenterY();
        double point2X = point2.getCircle().getCenterX();
        double point2Y = point2.getCircle().getCenterY();
        l.setStartX(point1X);
        l.setStartY(point1Y);
        l.setEndX(point2X);
        l.setEndY(point2Y);
        l.setFill(BLUE);
        l.toBack();
    }
    
    public Line getLine(){
        return l;
    }    
}