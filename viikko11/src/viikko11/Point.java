/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;


import javafx.scene.input.MouseEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;


/**
 *
 * @author p7973
 */
public class Point {
    String name;
    boolean memory = false;
    final Circle c = new Circle();
    private ShapeHandler sh;

    public Point(MouseEvent event){
            c.setCenterX(event.getX());
            c.setCenterY(event.getY());
            c.setRadius(10);
            c.setFill(Color.MAROON);
            c.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    System.out.print("Hei olen piste.");
                    memory = true;
                }
            });
           
    }
    
    public Circle getCircle(){
        return c;
    }
    public boolean getMemory(){
        return memory;
    }
    public void setMemory(boolean m){
        memory = m;
    }

    
}
