/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;


import javafx.scene.input.MouseEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private AnchorPane base;
    private ShapeHandler sh;
    private Line line;
    @FXML
    private CheckBox choose;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sh = ShapeHandler.getInstance();
        //Line line = new Line();
    } 
    
    @FXML
    public void baseMouseClicked(MouseEvent event) {
        if (event.getButton().toString() == "PRIMARY") {
            System.out.println("klik 1");
            Point p = new Point(event);
            sh.addList(p);
            base.getChildren().add(p.getCircle());
            System.out.println(p.getCircle().getCenterX());
            
        }
        else if(event.getButton().toString() == "SECONDARY"){
            
        }
        boolean c = choose.isSelected();
        sh.checkList(base, c);        

    }
}
