 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viiikko10;

import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;

/**
 *
 * @author p7973
 */
public class FXMLWebViewController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField URLinput;
    @FXML
    private WebView webWindow;
    @FXML
    private Button refreshButton;
    @FXML
    private Button prevPage;
    @FXML
    private Font x1;
    @FXML
    private Button nexPage;
    @FXML
    private Button sendOrder;
    @FXML
    private Button undoOrder;
    


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    public void startWebView(ActionEvent event) {
        try{
            if(URLinput.getText().contains("index2.html")){
                webWindow.getEngine().load(getClass().getResource("index2.html").toExternalForm());
            }
            else{
                webWindow.getEngine().load("https://"+URLinput.getText());
                URLinput.clear();
            }
        }catch(Exception e){
            
        }
    }

    @FXML
    private void RefreshAction(ActionEvent event) {
        webWindow.getEngine().getLocation();
    }

    @FXML
    private void prevPageAction(ActionEvent event) {
        webWindow.getEngine().getHistory().go(-1);
    }

    @FXML
    private void nexPageAction(ActionEvent event) {
        webWindow.getEngine().getHistory().go(1);
    }

    @FXML
    private void sendOrderAction(ActionEvent event) {
        webWindow.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void undoAction(ActionEvent event) {
        webWindow.getEngine().executeScript("initialize()");
    }
    
}
