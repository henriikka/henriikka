
package viikko4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import java.io.IOException;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 31.5.2019
 */
public class ReadAndWriteIO {
    
    public ReadAndWriteIO(){
        try {

            BufferedReader in;
            BufferedWriter out;
            in = new BufferedReader(new FileReader("zipinput.zip"));
            //out = new BufferedWriter(new FileWriter("outputEx.txt"));
            String inputLine;
            while((inputLine = in.readLine()) != null) {
               System.out.println(inputLine);
            }
            in.close();
        } catch (IOException ex) {
            System.out.println("File not found!");
        }
    }
}