//import java.util.ArrayList;
package teht3.pkg1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *Henriikka Harinen 0520995
 * NetBeans IDE 8.1
 * 29.5.2018
 */
public class BottleDispenser {
    private int bottles;
    //private Bottle[] bottle_array;
    private double money;
    private int choise;
    private ArrayList <Bottle> list = new ArrayList();
    
    public void listBottles() {
        list.add(new Bottle());
        list.add(new Bottle("Pepsi Max",1.5,2.2));
        list.add(new Bottle("Coca-Cola Zero",0.5,2.0));
        list.add(new Bottle("Coca-Cola Zero",1.5,2.5));
        list.add(new Bottle("Fanta Zero",0.5,1.95));
        list.add(new Bottle("Fanta Zero",0.5,1.95));
    }           
    public void printList(){
        for (int i = 0; i < list.size(); i++){
        System.out.println((i+1)+". "+"Nimi: "+list.get(i).getName());
        System.out.println("	Koko: "+list.get(i).getSize()+"	Hinta: "+list.get(i).getPrice());        
        }
    
    }
    
    public void buyBottle() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Valintasi: ");
        int num = scan.nextInt();                    
        Bottle b = list.get(num-1);
        if (money < b.getPrice()){
            System.out.println("Syötä rahaa ensin!");
        }
        else if(bottles < 1) {
            System.out.println("Pullot ovat loppuneet!");
	}else{ 
            //bottles -= 1;
            list.remove(num-1);
            money -= b.getPrice();
            System.out.println("KACHUNK! " + b.getName() + " tipahti masiinasta!");
        }
    }
    
    public BottleDispenser() {
        bottles = 5;
        money = 0;
        //bottle_array = new Bottle[bottles];
        //for(int i = 0; i<bottles; i++){
          //  bottle_array[i] = new Bottle();
        //}
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void returnMoney() {
        System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %4.2f€\n",money);
        money = 0;
       }
}



