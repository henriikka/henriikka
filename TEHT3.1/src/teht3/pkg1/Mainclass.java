
package teht3.pkg1;
//import java.util.ArrayList;

import java.util.Scanner;

/**
 *Henriikka Harinen 0520995
 * NetBeans IDE 8.1
 * 29.5.2018
 */
 public class Mainclass {
    public static void main(String[] args){
        BottleDispenser b = new BottleDispenser();
        b.listBottles();
        while(true){
            System.out.println("*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            Scanner scan = new Scanner(System.in);
            System.out.print("Valintasi: ");
            int choice = scan.nextInt();                    
            
            if (choice == 1){
                b.addMoney();
            }
            if (choice == 2){
                b.printList();
                b.buyBottle();
            }
            if (choice == 3){
                b.returnMoney();
            }
            if (choice == 4){
                b.printList();
            }
            if (choice == 0){
                break;
            }
        }
    }
 }
