/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication9;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

/**
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private Button buyButton;
    @FXML
    private Button returnButton;
    @FXML
    private ComboBox<String> sodaButton;
    @FXML
    private ComboBox<String> sizeButton;
    @FXML
    private Font x1;
    @FXML
    private Slider chooseMoney;
    @FXML
    private ImageView greenBottle;
    @FXML
    private Label countSpace;
    
    private BottleDispenser bd;
    @FXML
    private Label showMoneys;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bd = BottleDispenser.getInstance();
    } 
    

    @FXML
    private void buyBottonAction(ActionEvent event) {
        greenBottle.setVisible(true);
        bd.buyBottle();

    }

    @FXML
    private void putMoneyToWindow(MouseEvent event) {
        countSpace.setText(Double.toString(chooseMoney.getValue()));
    }

    @FXML
    private void addMoneyAction(ActionEvent event) {
    //showMoneys.setText(showMoneys.getText() + countSpace.getText());

    }

   

    @FXML
    private void addMoneyAction(MouseEvent event) {
    }

    @FXML
    private void showSodaList(MouseEvent event) {
        ArrayList <String> list = new ArrayList();
        list.add("Pepsi Max");
        list.add("CocaCola Zero");
        list.add("Fanta Zero");
        
        sodaButton.setItems((ObservableList<String>) list);
    }

    @FXML
    private void putMoneyToWindow(DragEvent event) {
    }

   











}
  
