
import java.util.ArrayList;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 1.6.2018
 */
public class Car {
    ArrayList list = new ArrayList();
    public Car(){
        Body b = new Body();
        list.add(b);        
        Chassis c = new Chassis();
        list.add(c);
        Engine e = new Engine();
        list.add(e);
        Wheel w1 = new Wheel();
        list.add(w1);
        Wheel w2 = new Wheel();
        list.add(w2);
        Wheel w3 = new Wheel();
        list.add(w3);
        Wheel w4 = new Wheel();
        list.add(w4);
    }    
    
    public void print(){
        System.out.println("Autoon kuuluu:");
        System.out.println("Body\nChasis\nEngine\n4 Wheel");
        }
    }
class Wheel{ 
    String name;
    public Wheel(){ 
        name = "Wheel";
        System.out.println("Valmistetaan: "+name);
    }
    public String getName(){
        return name;
    }    
} 
class Chassis{ 
    String name;
    public Chassis(){
        name = "Chassis";
        System.out.println("Valmistetaan: "+name);
    }
    public String getName(){
        return name;
    }
}    

class Engine{
    String name;
    public Engine(){
        name = "Engine";
        System.out.println("Valmistetaan: "+name);
    }
    public String getName(){
        return name;
    }
} 
    
class Body{ 
    String name;
    public Body(){
        name = "Body";
        System.out.println("Valmistetaan: "+name);
    } 
    public String getName(){
        return name;
    }
}