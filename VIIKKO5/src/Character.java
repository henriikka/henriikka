
import java.util.Scanner;


/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 1.6.2018
 */
public class Character {
    String character;
    public Character(){
        System.out.println("Valitse hahmosi: ");
        System.out.println("1) Kuningas\n2) Ritari\n3) Kuningatar\n4) Peikko");

        Scanner scan = new Scanner(System.in);
        System.out.print("Valintasi: ");
        int choice = scan.nextInt();
            if (choice == 1){
                character = "King";
            }
            if (choice == 2){
                character = "Knight";
            }            
            if (choice == 3){
                character = "Queen";
            }
            if (choice == 4){
                character = "Troll";
            }
    }
    public String getCharacter(){
        return character;
    }
}
    


class Weapon{
    String weapon;
    public Weapon(){
        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi\n2) Kirves\n3) Miekka\n4) Nuija");

        Scanner scan = new Scanner(System.in);
        System.out.print("Valintasi: ");
        int choice = scan.nextInt();
            if (choice == 1){
                weapon = "Knife";
            }
            if (choice == 2){
                weapon = "Axe";
            }            
            if (choice == 3){
                weapon = "Sword";
            }
            if (choice == 4){
                weapon = "Club";
            }
    }
    public String getWeapon(){
        return weapon;
    }
}