.open harjoitustyo.db
.headers on
.mode column

CREATE TABLE "automaatti"("automaattiID"	INTEGER         PRIMARY KEY AUTOINCREMENT,
                          "nimi"                VARCHAR(30)     NOT NULL, 
                          "osoite"              VARCHAR(30)     NOT NULL,
			  "city"                VARCHAR(30)     NOT NULL,
			  "postinumero"         VARCHAR(30)	NOT NULL,
                          "aukioloaika"         VARCHAR(30)     NOT NULL,
			  "long" 		VARCHAR(30)	NOT NULL,
			  "lat" 		VARCHAR(30)	NOT NULL
);


CREATE TABLE "paketti"  ("pakettiID"		INTEGER         PRIMARY KEY AUTOINCREMENT,
			 "esineID"		INTEGER		NOT NULL,
			 "luokka"		INTEGER		NOT NULL,

			 CHECK("luokka" IN (1,2,3)),
			 FOREIGN KEY ("esineID") REFERENCES "esine"("esineID")
);

CREATE TABLE "esine"   	("esineID"		INTEGER         PRIMARY KEY AUTOINCREMENT,
			 "nimi"                	VARCHAR(30)     NOT NULL,
			 "sarkyva"              BOOLEAN	        NOT NULL,
			 "paino"		REAL		NOT NULL,
                         "koko"                	REAL	     	NOT NULL,
			 CHECK("paino" > 0),
			 CHECK("koko" > 0)
); 

CREATE TABLE "lahetys" 	("lahetysID"	 	INTEGER         PRIMARY KEY AUTOINCREMENT,
			 "lahtoID"		INTEGER		NOT NULL,
			 "loppuID"              INTEGER		NOT NULL,
			 "pakettiID"		INTEGER		NOT NULL,
			 
			 FOREIGN KEY ("lahtoID") REFERENCES "automaatti"("automaattiID"),
			 FOREIGN KEY ("loppuID") REFERENCES "automaatti"("automaattiID"),
			 FOREIGN KEY ("pakettiID") REFERENCES "paketti"("pakettiID")
); 
INSERT INTO "esine"("nimi","sarkyva","paino","koko") VALUES ('maljakko', 'True', 1.5, 1000);
INSERT INTO "esine"("nimi","sarkyva","paino","koko") VALUES ('kivi', 'False', 7, 1000);
INSERT INTO "esine"("nimi","sarkyva","paino","koko") VALUES ('jalkapallo', 'False', 0.3, 1000);
INSERT INTO "esine"("nimi","sarkyva","paino","koko")VALUES ('housut', 'False', 0.5, 1000);

CREATE TABLE "kuitti"	("kuittiID"		INTEGER		PRIMARY KEY AUTOINCREMENT,
			 "pakettiID"		INTEGER		NOT NULL,
			 "aika"			VARCHAR(30)	NOT NULL
);