/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javafx.scene.control.ListView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private ComboBox<String> chooseTown;
    @FXML
    private Button addToMap;
    @FXML
    private Button clearMap;
    @FXML
    private Button createPacket;
    @FXML
    private Button clearRoutes;
    @FXML
    private Pane createPane;
    @FXML
    private RadioButton firstClass;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton thirdClass;
    @FXML
    private CheckBox easyBreak;
    @FXML
    private TextField packetWeight;
    @FXML
    private TextField packetSize;
    @FXML
    private TextField packetName;
    @FXML
    private ComboBox<String> itemMenu;
    @FXML
    private Button readyButton;
    @FXML
    private WebView webView;
    @FXML
    private TextArea infoBox;
    @FXML
    private ComboBox<String> startRoute;
    @FXML
    private ComboBox<String> endRoute;
    @FXML
    private ComboBox<String> startRouteAutomat;
    @FXML
    private ComboBox<String> endRouteAutomat;
    @FXML
    private Button cancelButton;
    @FXML
    private Button infoButton;
    DataBuilder D = new DataBuilder();
    SmartPost S = new SmartPost();
    Receipt R = new Receipt();
    @FXML
    private Button refreshFields;
    @FXML
    private ToggleGroup classes;
    @FXML
    private Button createNewItem;
    @FXML
    private Pane warning;
    @FXML
    private ListView<Packet> packetList;
    @FXML
    private Button removeSelectedPacket;
    @FXML
    private Label classWarning;
    @FXML
    private Pane senWarning;
    @FXML
    private Button makeReceipt;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        chooseTown.setItems(FXCollections.observableList(S.getCityName()));
        startRoute.setItems(FXCollections.observableList(S.getCityName()));
        endRoute.setItems(FXCollections.observableList(S.getCityName()));
        itemMenu.setItems(FXCollections.observableList(D.getItems()));
        D.getPacketsFromDB();
        packetList.setItems(FXCollections.observableList(D.getPackets()));
    }    

    @FXML
    private void createPacketAction(ActionEvent event) {
        createPane.setVisible(true);
    }

    @FXML
    private void sendAction(ActionEvent event) throws IOException{
        ArrayList<String> distList = D.geoPoints(startRouteAutomat.getValue(), endRouteAutomat.getValue());
        double distance;
        distance = (double) webView.getEngine().executeScript("document.getDistance("+distList+")");
        System.out.println(distance);
        boolean checkDist = true;
        String listName;
        Packet packet = packetList.getSelectionModel().getSelectedItem();
        if (!packetList.getSelectionModel().isEmpty() && startRoute.getValue() != null && endRoute.getValue() != null && startRouteAutomat.getValue() != null && endRouteAutomat.getValue() != null){
            createNewItemAction(event);
            String pack = packetList.getSelectionModel().getSelectedItem().toString();
            String postClass = pack.substring(pack.length()-1);
            int lk = Integer.parseInt(postClass);            
            
            if (lk == 1 && distance > 150.0){
                checkDist = false;
            }
            if(packet != null && checkDist == true && startRouteAutomat.getValue() != null && endRouteAutomat.getValue() != null){ 
                String startName = startRouteAutomat.getValue();
                String endName = endRouteAutomat.getValue();
                listName = packet.toString();
                String item  = listName.substring(0, listName.length()-2);
                System.out.println(item);
                D.getPacketID(item);
                D.addShipmentToDB(startName, endName, item);           
                System.out.println(startRouteAutomat.getValue().toString());
                ArrayList<String> drawList = D.geoPoints(startRouteAutomat.getValue(), endRouteAutomat.getValue());
                System.out.println(drawList.get(0));
                webView.getEngine().executeScript("document.createPath("+drawList+", \"red\", "+lk+")");
                removeSelectedPacketAction(event);
                createPane.setVisible(false); 
            } else{
                senWarning.setVisible(true);
            }              
        }else{
            warning.setVisible(true);
        }
        itemMenu.setValue(null);
        packetName.clear();
        packetWeight.clear();
        packetSize.clear();
        easyBreak.setSelected(false);
        startRoute.setValue(null);
        endRoute.setValue(null);
        startRouteAutomat.setValue(null);
        startRouteAutomat.getItems().clear();
        endRouteAutomat.getItems().clear();
        endRouteAutomat.setValue(null);
                          
    }

    @FXML
    private void infoBoxAction(ActionEvent event) {
        infoBox.setVisible(true);
    }

    @FXML
    private void closeBox(MouseEvent event) {
        infoBox.setVisible(false);
    }

    @FXML
    private void addPointsAction(ActionEvent event) {
        String chosenCity = chooseTown.getValue();        
        String sql = "SELECT osoite, postinumero, city, nimi ,aukioloaika FROM automaatti WHERE city = ?";
        try {
            Connection conn = D.connect();
            PreparedStatement pstmt  = conn.prepareStatement(sql);
            pstmt.setString(1,chosenCity);    
            ResultSet rs  = pstmt.executeQuery();            
            while (rs.next()) {
                String ad = rs.getString("osoite");
                String co = rs.getString("postinumero");
                String ci = rs.getString("city");
                String av = rs.getString("aukioloaika");
                String n = rs.getString("nimi");
                webView.getEngine().executeScript("document.goToLocation(\""+ad+", "+co+", "+ci+"\",\""+n+", "+av+"\", \"red\" )");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
    
    }

    @FXML
    private void setAutomats(MouseEvent event) { 
        if (startRoute.getValue() != null){
            String city = startRoute.getValue();
            startRouteAutomat.setItems(FXCollections.observableList(D.getAutomat(city)));
        }
    }

    @FXML
    private void setAutomatsEnd(MouseEvent event) {
        if (endRoute.getValue() != null){
            String city = endRoute.getValue();
            endRouteAutomat.setItems(FXCollections.observableList(D.getAutomat(city)));
        }    }

    @FXML
    private void clearMapAction(ActionEvent event) {
        webView.getEngine().reload();
        chooseTown.setValue(null);
    }


    @FXML
    private void refreshAction(ActionEvent event) {
        itemMenu.setValue(null);
        packetName.clear();
        packetWeight.clear();
        packetSize.clear();
        easyBreak.setSelected(false);       
    }

    @FXML
    private void cancelAction(ActionEvent event) {
        itemMenu.setValue(null);
        packetName.clear();
        packetWeight.clear();
        packetSize.clear();
        easyBreak.setSelected(false);
        startRoute.setValue(null);
        endRoute.setValue(null);
        startRouteAutomat.setValue(null);
        endRouteAutomat.setValue(null);
        startRouteAutomat.getItems().clear();
        endRouteAutomat.getItems().clear();
        createPane.setVisible(false);
    }

    @FXML
    private void createNewItemAction(ActionEvent event) {        
        String name = packetName.getText();
        if (!name.isEmpty()){
        Boolean breakable = easyBreak.isSelected();        
            try{
                float weight = Float.parseFloat(packetWeight.getText());
                float size = Float.parseFloat(packetSize.getText());
                D.createNewItem(name,breakable,weight,size);
            } catch (Exception e) {
                warning.setVisible(true);
                itemMenu.setValue(null);
                packetName.clear();
                packetWeight.clear();
                packetSize.clear();
                easyBreak.setSelected(false);
            }
        } else {
            itemMenu.setValue(null);
            packetName.clear();
            packetWeight.clear();
            packetSize.clear();
            easyBreak.setSelected(false);
        }
        itemMenu.setItems(FXCollections.observableList(D.getItems()));
        refreshAction(event);
    }

    @FXML
    private void closeWarning(MouseEvent event) {
        warning.setVisible(false);        
    }

    @FXML
    private void createNewPacketAction(ActionEvent event) {        
        int postClass;
        String name; 
        if ((name = itemMenu.getValue()) == null){
            name = packetName.getText();
            createNewItemAction(event);
            itemMenu.setValue(null);
            packetName.clear();
            packetWeight.clear();
            packetSize.clear();
            easyBreak.setSelected(false);
        }
        
        String radioButtonText = ((RadioButton)classes.getSelectedToggle()).getText();
        if (radioButtonText.contains("1. Luokka")){
            postClass = 1;             
        }
        else if (radioButtonText.contains("2. Luokka")){
            postClass = 2;             
        }
        else if (radioButtonText.contains("3. Luokka")){
            postClass = 3;             
        } else{
            postClass = -1;
        }
        boolean breakable = easyBreak.isSelected();
        D.setPacketToDB(name,postClass,breakable);
        
        packetList.setItems(FXCollections.observableList(D.getPackets()));  
    }

    @FXML
    private void removeSelectedPacketAction(ActionEvent event) {           
            String listName;
            Packet packet = packetList.getSelectionModel().getSelectedItem();
            if(packet != null){ 
                listName = packet.toString();
                String nimi = listName.substring(0, listName.length()-2);
                D.getItemId(nimi);
                D.removePacketFromDB(nimi);
                System.out.println(nimi);
                packetList.setItems(FXCollections.observableList(D.getPackets()));  
        }
    }

    @FXML
    private void clearRoutesAction(ActionEvent event) {
        webView.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void closeClassWarning(ContextMenuEvent event) {
        classWarning.setVisible(false);
    }

    @FXML
    private void closeSendWarning(MouseEvent event) {
        senWarning.setVisible(false);
    }
}
