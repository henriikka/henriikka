
import java.util.ArrayList;
/**
 *Henriikka Harinen 0520994
 * 
 */
public class SmartPost {
    
    ArrayList<Automat> automatList = new ArrayList();
    private final XMLreader X;    
    public SmartPost() {
        X = new XMLreader();
        automatList = X.getCurrentData();       
    }

    public ArrayList getList(){
        return automatList;
    }
    
    public ArrayList getCityName(){
        ArrayList<String> cityNameList = new ArrayList();
        for (Automat a: automatList){
            String city = a.getCity().toUpperCase();
            if(!cityNameList.contains(city)){
                cityNameList.add(city);
            }
        }
        return cityNameList;       
    }
}
