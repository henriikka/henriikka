
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * Henriikka Harinen
 */
public class XMLreader {
    private Document doc;
    Connection con = null;
    public XMLreader() {
        String s = null;
        try {
            s = gatherData();
        } catch (IOException ex) {
            Logger.getLogger(XMLreader.class.getName()).log(Level.SEVERE, null, ex);
        }
        parseXML(s);
    }    
    private String gatherData() throws MalformedURLException, IOException{
        URL url = new URL("http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT");
        String total = "";

        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputLine;
            while((inputLine = in.readLine()) != null){
                total += inputLine+"\n";
            }
        }
        return total;
    }   
    private void parseXML(String total){
        
        try {
            DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = (Document) dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }      
    public ArrayList getCurrentData() {
        ArrayList<Automat> automatList = new ArrayList();
        try {
            NodeList nodes = doc.getElementsByTagName("item");
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(XMLreader.class.getName()).log(Level.SEVERE, null, ex);
            }
            String url2 = "jdbc:sqlite:harjoitustyo.db";
            con = DriverManager.getConnection(url2);
            Statement stm = con.createStatement();
            stm.executeUpdate("DELETE FROM \"automaatti\"");
            String sql = "INSERT INTO automaatti(nimi, osoite, city, postinumero, aukioloaika, long, lat)"
                    + "VALUES(?,?,?,?,?,?,?)";
            for(int i = 1; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                Element e = (Element) node;
                String name = getValue("name",e);
                String address = getValue("address",e);
                String city = getValue("city",e);
                String availability = getValue("availability",e);
                String lng = getValue("lng",e);
                String lat = getValue("lat",e);
                String postalcode = getValue("postalcode",e);
                
                Automat a = new Automat(name,address,city,postalcode,availability,lng,lat);
                automatList.add(a);
                PreparedStatement pstmt = con.prepareStatement(sql); 
                pstmt.setString(1, name);
                pstmt.setString(2, address);
                pstmt.setString(3, city);
                pstmt.setString(4, postalcode);
                pstmt.setString(5, availability);
                pstmt.setString(6, lng);
                pstmt.setString(7, lat);
                pstmt.executeUpdate();               
            }
            con.close();            
        } catch (SQLException ex) {
            Logger.getLogger(XMLreader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return automatList;
    }   
    private String getValue(String tag, Element e) {
        return ((Element)(e.getElementsByTagName(tag).item(0))).getTextContent();
    }                            
}
