import java.util.ArrayList;
/**
 *
 * @author p7973
 */
public class PostClasses {
    ArrayList<Packet> packets = new ArrayList();
    boolean create;
    private static PostClasses post = null;
    public static PostClasses getInstant() {
        if (post == null){
            post = new PostClasses();
        }
        return post;
    }
    
    public Packet PacketCreator(String name, int postClass, int itemID, double size, double weight, boolean breakable) {
        Packet packet = null;
        boolean check = true;
        create = false;        
        if (name.isEmpty()) {
            check = false;
        }
        else if (itemID <= 0) {
            check = false;
        }        
        if (check && postClass == 1) {
            if (size <= 64000 && weight <= 10 && breakable == false){
                packet = new FirstClass(name, postClass, itemID);
                packets.add(packet);
                create = true;                
            }else {
                System.out.println("virhe");
            }
        }
        else if (check && postClass == 2) {
            if (size <= 8000 && weight <= 5){
                packet = new SecondClass(name, postClass, itemID);
                packets.add(packet);
                create = true;
            }else{
                System.out.println("virhe");
            }
        }
        else if (check && postClass == 3) {
            if (size <= 1000000 && weight <= 50 && breakable == false){
                packet = new ThirdClass(name, postClass, itemID);
                packets.add(packet);
                create = true;
            }
            else {
                System.out.println("virhe");                
            }
        } 
        return packet;
    } 

    public ArrayList getPacketList(){
        return packets;
    }  
    public boolean getCreate(){
        return create;
    }
}


class Packet {
    protected String name;
    protected int postClass;
    protected int itemID;
    
    @Override
    public String toString(){
        return name+","+postClass;
    }
    
    public String getName(){
        return name;
    }
    public int getItemID(){
        return itemID;
    }
}

class FirstClass extends Packet{
    public FirstClass(String name, int postClass, int itemID) {
       this.name = name;
       this.postClass = postClass;
       this.itemID = itemID;
    }
}
class SecondClass extends Packet{
    public SecondClass(String name, int postClass, int itemID) {
       this.name = name;
       this.postClass = postClass;
       this.itemID = itemID;        
    }
}
class ThirdClass extends Packet{
    public ThirdClass(String name, int postClass, int itemID) {
       this.name = name;
       this.postClass = postClass;
       this.itemID = itemID;        
    }
    
}
