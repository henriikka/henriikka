
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * Henriikka Harinen 0520994
 */
public class DataBuilder {
    
    PostClasses pc = PostClasses.getInstant();
    ArrayList shipments = new ArrayList();
    public Connection connect() {
        String url = "jdbc:sqlite:harjoitustyo.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }    
    public void createNewItem(String name, Boolean breakable, float weight, float size){ //lisää uuden esineen 'esine' tauluun
        String sql = "INSERT INTO esine(nimi, sarkyva, paino, koko) VALUES (?,?,?,?)";         
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setBoolean(2, breakable);
            pstmt.setFloat(3, weight);
            pstmt.setFloat(4, size);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }        
    }    
    public ArrayList getAutomat(String city){
        ArrayList<String> list = new ArrayList();
        String sql = "SELECT nimi FROM automaatti WHERE city = ?";
        try (Connection conn = this.connect();
            PreparedStatement pstmt  = conn.prepareStatement(sql)){            
            pstmt.setString(1, city);
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("nimi"));               
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        return list;
    }    
    public ArrayList getItems(){
        ArrayList items = new ArrayList();
        String sql = "SELECT nimi FROM esine";
        try (Connection conn = this.connect();
            Statement stmt  = conn.createStatement();  
            ResultSet rs  = stmt.executeQuery(sql)){
            while (rs.next()) {
                items.add(rs.getString("nimi")); 
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        return items;
    }
    public ArrayList<String> geoPoints(String start, String end){
        ArrayList<String> draw = new ArrayList();
        String sql = "SELECT long, lat FROM automaatti WHERE nimi = ?";
        try (Connection conn = this.connect();
            PreparedStatement pstmt  = conn.prepareStatement(sql)){            
            pstmt.setString(1, start);
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                draw.add(rs.getString("lat"));
                draw.add(rs.getString("long"));    
            }
            pstmt.setString(1, end);
            rs  = pstmt.executeQuery();
            while (rs.next()) {
                draw.add(rs.getString("lat"));
                draw.add(rs.getString("long"));  
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        return draw;
    }
    public void setPacketToDB(String name, int postClass, boolean breakable){
        String sql2 = "INSERT INTO paketti(esineID,luokka) VALUES (?,?)";
        String sql1 = "SELECT esineID, sarkyva, paino, koko FROM esine WHERE nimi = ?";
        int itemID = -1;
        int weight = -1;
        int size = -1;
        
        
        try { 
            Connection conn = this.connect();
            PreparedStatement pstmt1 = conn.prepareStatement(sql1);
            
            pstmt1.setString(1, name);
            ResultSet rs  = pstmt1.executeQuery();
            while (rs.next()) {
                itemID = rs.getInt("esineID");
                weight = rs.getInt("paino");
                size = rs.getInt("koko");
                breakable = rs.getBoolean("sarkyva");
                
            }
            pc.PacketCreator(name, postClass, itemID, size, weight, breakable);
            
            if (pc.getCreate() == true){
                PreparedStatement pstmt2 = conn.prepareStatement(sql2);
                pstmt2.setInt(1, itemID);
                pstmt2.setInt(2, postClass);
                pstmt2.executeUpdate();
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }       
    }    
    public ArrayList getPackets(){
        return pc.getPacketList();
    }    
    public void getPacketsFromDB(){
        String sql = "SELECT paketti.luokka AS luokka, paketti.esineID AS esineID, esine.koko AS koko, esine.paino AS paino, esine.sarkyva AS sarkyva, esine.nimi AS nimi "
                + "FROM paketti JOIN esine ON paketti.esineID = esine.esineID";
        String name;
        int postClass;
        int itemID;
        double size;
        double weight;
        boolean breakable;
        
        try (Connection conn = this.connect();
            Statement stmt  = conn.createStatement();  
            ResultSet rs  = stmt.executeQuery(sql)){

            while (rs.next()) {
                name = rs.getString("nimi");
                postClass = rs.getInt("luokka");
                itemID = rs.getInt("esineID");
                size = rs.getDouble("koko");
                weight = rs.getDouble("paino");
                breakable = rs.getBoolean("sarkyva");
                pc.PacketCreator(name, postClass, itemID, size, weight, breakable);

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }  
    }
    public int getItemId(String name){
        String sql = "SELECT esineID FROM esine WHERE nimi = ?";
        int itemID = -100;
        try (Connection conn = this.connect();
            PreparedStatement pstmt  = conn.prepareStatement(sql)){
            pstmt.setString(1,name);
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                itemID = rs.getInt("esineID");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return itemID;
    }
    public void removePacketFromDB(String name){
        String sql = "DELETE FROM paketti WHERE esineID = ?";
        
        int esineID = getItemId(name);
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, esineID);
            
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        for (int i = 0; i < pc.packets.size(); i++){
            if(pc.packets.get(i).getName().equals(name)){
                pc.packets.remove(i);
            }
        }
        
    }  
    public int getAutomatID(String name){
        String sql = "SELECT automaattiID FROM automaatti WHERE nimi = ?";
        int ID = 0;
        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){           
            pstmt.setString(1,name);
            ResultSet rs  = pstmt.executeQuery();
            
            while (rs.next()) {
                ID = rs.getInt("automaattiID");
                
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ID;
    }
    public int getPacketID(String name){
        int itemID = 0;
        for (int i = 0; i < pc.packets.size(); i++){
            if(pc.packets.get(i).getName().equals(name)){
                itemID = pc.packets.get(i).getItemID();
            }
        }
        String sql = "SELECT pakettiID FROM paketti WHERE esineID = ?";
        int packetID = 0;
        
        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){           
            pstmt.setInt(1,itemID);            
            ResultSet rs  = pstmt.executeQuery();
            while (rs.next()) {
                packetID = rs.getInt("pakettiID");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return packetID;
    }
    public void addShipmentToDB(String startName, String endName, String itemName) throws IOException{
        String sql = "INSERT INTO lahetys(lahtoID,loppuID,pakettiID) VALUES (?,?,?)";
        
        int startID = getAutomatID(startName);
        int endID = getAutomatID(endName);
        int packetID = getPacketID(itemName);
        try(Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setInt(1,startID);
            pstmt.setInt(2,endID);
            pstmt.setInt(3,packetID);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } 
        shipments.add("Lähtö automaatti: "+startName+", Pääte automaatti: "+endName+", Paketti ID: "+packetID); 
        getShipmentList();
        Receipt R = new Receipt();
        R.MakeReceipt(shipments);
        String date = R.getDate().toString();
        System.out.println(date);
        String sql2 = "INSERT INTO kuitti(pakettiID,aika) VALUES (?,?)";
        try(Connection conn = this.connect();
                PreparedStatement pstmt2 = conn.prepareStatement(sql2)){
            pstmt2.setInt(1,packetID);
            pstmt2.setString(2, date);
            pstmt2.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public ArrayList getShipmentList(){
        System.out.println(shipments);
        return shipments;
    }
}
