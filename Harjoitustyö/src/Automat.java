/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * Henriikka Harinen
 */
public class Automat {
    String name;
    String address;
    String city;
    String availability;
    String lng;
    String lat;
    String code;
    
    public Automat(String n, String ad, String c, String co, String av, String l, String la){
        name = n;
        address = ad;
        city = c;
        availability = av;
        lng = l;
        lat = la;
        code = co;
    }
    
    @Override
    public String toString() {
        return address;
    }    
    public String getCity(){
        return city;
    }
    public String getName(){
        return name;
    }    
}
