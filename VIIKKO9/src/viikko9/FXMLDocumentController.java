/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;

/**
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField startingTime;
    @FXML
    private TextField showDate;
    @FXML
    private ComboBox<Theater> chooseTheater;
    @FXML
    private TextField endingTime;
    @FXML
    private Button searchName;
    @FXML
    private Font x1;
    @FXML
    private TextField addName;
    @FXML
    private Button listMovies;
    @FXML
    private ListView<String> movieList;
    private LoadXML L = new LoadXML();

    private void handleButtonAction(ActionEvent event) {
                
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        chooseTheater.setItems(FXCollections.observableList(L.getList()));
        System.out.print(L.getList());
    }    

    @FXML
    private void listMoviesAction(ActionEvent event) {
        String sd = showDate.getText();
        String st = startingTime.getText();
        String et = endingTime.getText();
        Date date;
        date = L.date(sd);
        L.clearaus();
        L.listMovies(chooseTheater.getValue().toString(),date,st,et);       
        movieList.setItems(FXCollections.observableArrayList(L.getMovieNames()));
        
    }

    @FXML
    private void searchNameAction(ActionEvent event) throws IOException {
        String filmName;
        filmName = addName.getText();
        L.listWNames(filmName);
        movieList.setItems(FXCollections.observableArrayList(L.listWNames(filmName)));
    }



}
