/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * Henriikka Harinen 0520994
 */
public class LoadXML {
    private Document doc;
    ArrayList <Theater> list = new ArrayList();
    ArrayList movieNames = new ArrayList();
    ArrayList finalPart = new ArrayList();
    
    public LoadXML() {
        String s = null;
        try {
            s = gatherData();
        } catch (IOException ex) {
            Logger.getLogger(LoadXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        cinemaXML(s);
        getCurrentData();
    }
    private String gatherData() throws MalformedURLException, MalformedURLException, IOException{
        URL url = new URL("https://www.finnkino.fi/xml/TheatreAreas/");
        String total;
        total = "";

        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputLine;
            while((inputLine = in.readLine()) != null){
                total += inputLine+"\n";
            }
        }
        return total;
    }
    
    private void cinemaXML(String total){
        
        try {
            DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = (Document) dBuilder.parse(new InputSource(new StringReader(total)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }
    private void getCurrentData(){
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            list.add(new Theater(getValue("ID",e),getValue("Name",e)));
        }
    }
    
    private String getValue(String tag, Element e) {
        return ((Element)
            (e.getElementsByTagName(tag).item(0))).getTextContent();
        }
    public ArrayList getList(){
        return list;
    }
    
    public Date date(String sd){
        Date date;
        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        
        try {
            date = simpleDateFormat.parse(sd);
        } catch (ParseException ex) {
            date = new Date();
            //Logger.getLogger(LoadXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return date;
    }
    public void clearaus(){
        movieNames.clear();

    }
    public void listMovies(String x, Date d, String st, String et){
        Date strt = null;
        Date ed = null; 
        Date movieStart = null;
        String filmID = loopList(x);
        if (st.isEmpty()) {
            st = "00.00";
        }
        if (et.isEmpty()) {
            et = "23.59";
        }
        try {
            cinemaXML(gatheringData(filmID, d));
        } catch (IOException ex) {
            Logger.getLogger(LoadXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        NodeList nodes = doc.getElementsByTagName("Show");
        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String newTime = simpleDateFormat.format(d);
        String start = newTime + " " + st;
        String end = newTime + " " + et;
        SimpleDateFormat s = new SimpleDateFormat("dd.MM.yyyy HH.mm");
        try {
            strt = s.parse(start);
            ed = s.parse(end);
        } catch (ParseException ex) {
            strt = new Date();
            ed = new Date();
        }
        SimpleDateFormat s2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            String movieTime = getValue("dttmShowStart",e);
            movieTime = movieTime.replace("T", " ");
            try {
                movieStart = s2.parse(movieTime);
            } catch (ParseException ex) {
                Logger.getLogger(LoadXML.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (movieStart.before(strt) || movieStart.after(ed)) {
            }
            else {
                movieNames.add(getValue("Title",e)+getValue("dttmShowStart",e));
            }
        }
    }
    private String gatheringData(String fI, Date d) throws MalformedURLException, MalformedURLException, IOException{
        String pattern = "dd.MM.yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
       
        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area="+fI+"&dt="+simpleDateFormat.format(d));
        String total;
        total = "";

        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String inputLine;
            while((inputLine = in.readLine()) != null){
                total += inputLine+"\n";
            }
        }
        return total;
    }

    public String loopList(String x){
        for(int i = 0; i < list.size(); i++){
            if (list.get(i).toString().equals(x)){
                return list.get(i).getID();
            }
        }
        return "";
    }
    
    public ArrayList getMovieNames(){
        return movieNames;
    }
    private void gatheredData() throws MalformedURLException, MalformedURLException, IOException{

    URL url = new URL("https://www.finnkino.fi/xml/Schedule/");
    String total;
    total = "";

    try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
        String inputLine;
        while((inputLine = in.readLine()) != null){
            total += inputLine+"\n";
        }
    }
    cinemaXML(total);
    }
    public ArrayList listWNames(String s) throws IOException{
        finalPart.clear();
        gatheredData();
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            if (getValue("Title",e).contains(s)){
                finalPart.add((getValue("Theatre",e)+getValue("dttmShowStart",e)));
            }
        }
    return finalPart;
    }
}
