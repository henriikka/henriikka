/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko7.tehtävä.pkg5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author p7973
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button downloadButton;
    @FXML
    private Button saveButton;
    @FXML
    private TextArea inputField;
    @FXML
    private TextField documentName;
    @FXML
    private TextField documentName2;
    
    //@FXML
    @FXML
    private void downloadButtonAction(ActionEvent event) throws IOException {
        BufferedReader in;
        try {
            String inputLine;
            in = new BufferedReader(new FileReader(documentName2.getText()));
            while((inputLine = in.readLine()) != null) {
                inputField.setText(inputLine);
            }
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            inputField.setText("Tiedostoa ei löytynyt...");
        }
        
        documentName2.clear();
    }
    
    @FXML
    private void saveButtonAction(ActionEvent event) throws FileNotFoundException, IOException {
        BufferedWriter out;
        out = new BufferedWriter(new FileWriter(documentName.getText()));
        out.write(inputField.getText());
        out.close();
        documentName.clear();
        inputField.clear();
        
    
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    
}
