/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht2.pkg5;

import java.util.Scanner;

/**
 *Henriikka Harinen 0520994
 * NetBeans IDE 8.1
 * 29.5.2018
 */
public class Dog {
    private String name;
    private String speak;

    public Dog(){
        
            Scanner scan = new Scanner(System.in);
            System.out.print("Anna koiralle nimi: ");

            name = scan.nextLine();
            //name = name.trim();
            if (name.trim().isEmpty()){
            name = "Doge";
            }
            System.out.println("Hei, nimeni on "+name+"!");

            while(true){
                System.out.print("Mitä koira sanoo: ");
                speak = scan.nextLine();
                speak = speak.trim();
                if (speak.isEmpty()){
                    speak = "Much wow!";
                    System.out.println(name+": "+speak);

                } else{
                    System.out.println(name+": "+speak);
                    break;
                }
            }
    }        
}