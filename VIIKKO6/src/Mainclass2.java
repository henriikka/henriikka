
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;



/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */
public class Mainclass {

    public static void main(String[] args) {
        Bank2 b = new Bank2();
        while(true){
            System.out.print("\n");

            System.out.println("*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            Scanner scan = new Scanner(System.in);
            System.out.print("Valintasi: ");
            int choice = scan.nextInt();                    

                if (choice == 1){
                    b.addAccount();
                }

                if (choice == 2){
                    Scanner scand = new Scanner(System.in);
                    System.out.print("Syötä tilinumero: ");
                    String dnumber = scand.nextLine();                       
                    Scanner scanc = new Scanner(System.in);
                    System.out.print("Syötä rahamäärä: ");
                    String money = scanc.nextLine();                       
                    Scanner scanm = new Scanner(System.in);
                    System.out.print("Syötä luottoraja: ");
                    int credit = scand.nextInt(); 
                    //list.add("Tilinumero: "+dnumber);
                    //list.add("Rahamäärä: "+money);
                    System.out.println("Pankkiin lisätään: "+dnumber+","+money+","+credit);

                }

                if (choice == 3){
                    Scanner scand = new Scanner(System.in);
                    System.out.print("Syötä tilinumero: ");
                    String dnumber = scand.nextLine();                       
                    Scanner scanc = new Scanner(System.in);
                    System.out.print("Syötä rahamäärä: ");
                    String money = scanc.nextLine(); 
                    System.out.println("Talletetaan tilille: "+dnumber+" rahaa "+money);

                    
                }
                if (choice == 4){
                    Scanner scand = new Scanner(System.in);
                    System.out.print("Syötä tilinumero: ");
                    String dnumber = scand.nextLine();                       
                    Scanner scanc = new Scanner(System.in);
                    System.out.print("Syötä rahamäärä: ");
                    String money = scanc.nextLine(); 
                    System.out.println("Nostetaan tililtä: "+dnumber+" rahaa "+money);

                }
                if (choice == 5){
                    b.removeAccount();
                }
                if (choice == 6){
                    Scanner scansix = new Scanner(System.in);
                    System.out.print("Syötä tulostettava tilinumero: ");
                    String number = scansix.nextLine();    
                    System.out.println("Etsitään tiliää: "+number);
                }
                if (choice == 7){

                    System.out.println("Kaikki tilit:");
                }
                if (choice == 0){
                    break;
                }
                if (choice > 7){
                    System.out.println("Valinta ei kelpaa");
                }
        }      
    }
}
 