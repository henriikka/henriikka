package VIIKKO66;
import java.io.IOException;
import java.util.ArrayList;


/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */
public class Bank {
    Account na = new Account();
    
    
    public Bank() {
    }
    
    public void addNormalAccount() throws IOException {
        na.createNormalAccount();
    }
    
    public void addCreditAccount(String nmb, int a, int c) {
        System.out.println("Pankkiin lisätään: " + nmb + "," + a + "," + c);
    }
    
    public void removeAccount(String nmb) {
    	System.out.println("Tili poistettu.");
    }
    
    public void search(String nmb) {
        System.out.println("Etsitään tiliä: " + nmb);
    }
    
    public void printAll() {
        System.out.println("Kaikki tilit:");
    }
    
    public void deposit(String nmb, int a) {
        System.out.println("Talletetaan tilille: " + nmb + " rahaa " + a);
    }
    
    public void withdraw(String nmb, int a) {
        System.out.println("Nostetaan tililtä: " + nmb + " rahaa " + a);
    }
}
