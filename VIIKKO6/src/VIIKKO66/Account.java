
package VIIKKO66;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.in;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */ 
public class Account {
    String accountNumber;
    String line;
    int amount, credit, choice;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    public Account(){        
    }
    public void createNormalAccount() throws IOException{
        System.out.print("Syötä tilinumero: ");
        line = in.readLine();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(in.readLine());
        System.out.print("Tili luotu.");

    }
}
