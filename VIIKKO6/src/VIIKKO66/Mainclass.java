package VIIKKO66;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *Henriikka Harinen 0520994
 *NetBeans IDE 8.1
 * 4.6.2018
 */
public class Mainclass {
    
    public static void main(String[] args) throws IOException {
        String line;
        int amount, credit, choice;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Bank b = new Bank();
        while(true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili\n2) Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä");
            System.out.println("5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta");
            System.out.print("Valintasi: ");
            choice = Integer.parseInt(in.readLine());
            switch(choice) {
                case 0:
                    return;
                case 1:
                    b.addNormalAccount();
                    
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    line = in.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    amount = Integer.parseInt(in.readLine());
                    System.out.print("Syötä luottoraja: ");
                    credit = Integer.parseInt(in.readLine());
                    b.addCreditAccount(line, amount, credit);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    line = in.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    amount = Integer.parseInt(in.readLine());
                    b.deposit(line, amount);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    line = in.readLine();
                    System.out.print("Syötä rahamäärä: ");
                    amount = Integer.parseInt(in.readLine());
                    b.withdraw(line, amount);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    line = in.readLine();
                    b.removeAccount(line);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    line = in.readLine();
                    b.search(line);
                    break;
                case 7:
                    b.printAll();
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
        }
    }
    
}